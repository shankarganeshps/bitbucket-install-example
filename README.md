# Bitbucket add-on installation UX demo

This Bitbucket add-on demonstrates various ways to handle the Bitbucket add-on
installation experience.

See [the add-on website](https://bitbucket-install-example.aerobatic.io) for
more details.
